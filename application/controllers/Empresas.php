<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Empresas extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("EmpresasModel", "empresa");
	}

	public function index(){    
        if ($this->session->has_userdata('usuario')) {
			redirect(base_url().'dashboard');
			exit();
		}
		$this->load->view('login');
    }

	public function add(){
    	$this->load->view('empresas/adicionar');
    }

    public function listar(){
		$empresas = $this->empresa->listar();
		$dados = array("empresas" => $empresas);
        $this->load->view('empresas/listar', $dados);
	}



	public function adicionar(){
		$data = array(
			'nome' => $_POST['nome-empresa'],
			'nome_fantasia' => $_POST['nome-fantasia'],
			'cnpj' => $_POST['cnpj'],
			'status' => $status='Ativo',
		);
		
		$empresa = $this->empresa->adicionarEmpresa($data);
		redirect('Empresas/listar');
	}

	public function deletar($id){
		$id = $this->uri->segment(3);
		$empresa = $this->empresa->deletarEmpresa($id);
		redirect('Empresas/listar');
	}

	public function editar($id){
		$id = $this->uri->segment(3);

		$data = array(
			'nome' => $_POST['nome-empresa'],
			'nome_fantasia' => $_POST['nome-fantasia'],
			'cnpj' => $_POST['cnpj'],
			'status' => $status='Ativo',
		);

		$empresa = $this->empresa->editarEmpresa($id, $data);
		redirect('Empresas/listar');
	}	

	public function buscaPorId($id){
		$id = $this->uri->segment(3);

		$empresas = $this->empresa->buscaPorId($id);
		$dados = array("empresas" => $empresas);

        $this->load->view('empresas/editar', $dados);
	}
}