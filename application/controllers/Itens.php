<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Itens extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("ItensModel", "item");
    }
 
    // public function add($dados=''){
    //     $this->load->view('itens/adicionar', $dados);
    // }

    public function adicionar(){

		$data = array(
			'item'					=> $_POST['item'],
			'detalhes'				=> implode('|', $_POST['detalhes']),
			'observacoes'			=> $_POST['observacoes'],
			'tb_encaminhamento_id'	=> $_POST['idEncaminhamento'],
		);
    	$dados = $this->item->adicionarItens($data);
    	$this->load->view('itens/adicionar');
	}
	
	public function editar(){
		$data = array(
			'id_Item'				=> $_POST['id_Item'],
			'item'					=> $_POST['item'],
			'detalhes'				=> implode('|', $_POST['detalhes']),
			'observacoes'			=> $_POST['observacoes'],
			'tb_encaminhamento_id'	=> $_POST['idEncaminhamento'],
		);

		$item = $this->item->editarItem($data);
		//echo $this->session->set_flashdata('id_encaminhamento', $data['tb_encaminhamento_id']);
		$this->load->view('itens/editar');
	}

	public function buscaPorId($id){
		$id = $this->uri->segment(3);
		$itens = $this->item->buscaPorId($id);
		$dados = array(
			'itens'		=> $itens,
		);

		$this->load->view('itens/editar', $dados);
	}

	public function buscarItemModal(){
		$id = $_POST['id'];
		$itens = $this->item->buscarItem($id);
		
		echo json_encode($itens);
                    
	}

}