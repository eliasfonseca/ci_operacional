<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orcamentos extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("OrcamentosModel", "orcamento");
		$this->load->model('EmpresasModel', "empresa");
	}

	public function add(){
		$dados = array(
			"empresas" => $this->empresa->listar(),
			"orcamentos" => $this->orcamento->previaOrcamentos()
		);
		$this->load->view('orcamentos/adicionar', $dados);

		// echo '<pre>';
		// var_dump($dados);
		// exit();
	}

	public function adicionar(){
		$data = array(
			'valor_unitario' => $_POST['valor_unitario'],
			'valor_extra' => $_POST['valor_extra'],
			'observacoes' => $_POST['observacoes'],
			'status' => 'Ativo',
			'tb_empresas_id' => $_POST['empresa'],
			'tb_encaminhamento_id' => $_POST['idEncaminhamento'],
		);

		$this->orcamento->adicionarOrcamento($data);

		redirect('Orcamentos/add');
	}
}