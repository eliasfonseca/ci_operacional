<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Encaminhamentos extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("EncaminhamentosModel", "encaminhamento");
	}

	public function index(){    
        if ($this->session->has_userdata('usuario')) {
			redirect(base_url().'dashboard');
			exit();
		}
		$this->load->view('login');
	}
	
	public function add(){
		$this->load->view('encaminhamentos/adicionar');
	}

	public function adicionar(){
    	date_default_timezone_set('America/Sao_Paulo');
    	$data_registro = date('d/m/Y H:i:s', time());
		
		$data = array(
			'local' 			=> $_POST['local'],
			'data' 				=> $_POST['data'],
			'centro_custo' 		=> $_POST['centro_custo'],
			'tratamento' 		=> $_POST['tratamento'],
			'responsavel' 		=> $_POST['responsavel'],
			'cargo' 			=> $_POST['cargo'],
			'qtd_participantes' => $_POST['qtd_participantes'],
			'tb_usuarios_id' 	=> $this->session->userdata('usuario')->id,
			'servico' 			=> $_POST['servico'],
			'status'			=> 'Ativo',
			'data_registro'		=> $data_registro,
		);
		$this->db->trans_start();
		$ultimoId = $this->encaminhamento->adicionarEncaminhamento($data);

		$this->db->trans_complete();

		if ($ultimoId) {
			$this->session->set_userdata('idEncaminhamento',$ultimoId);
			$this->session->set_userdata('servico', $data['servico']);
			$this->session->set_flashdata("msgSuccess", "Encaminhamento salvo!");
			$this->load->view('itens/adicionar');
		}else {
			$this->session->set_flashdata("msgDanger", "O encaminhamento não foi salvo!");
			$this->load->view('itens/adicionar');
		}
	}

    public function listar(){
		$encaminhamento = $this->encaminhamento->listar();
		$dados = array("encaminhamentos" => $encaminhamento);
        $this->load->view('encaminhamentos/listar', $dados);
	}

	public function deletar($id){
		$id = $this->uri->segment(3);
		$encaminhamento = $this->encaminhamento->deletarEncaminhamento($id);
		redirect('Encaminhamentos/listar');
	}

	public function buscaPorId($id){
		$id = $this->uri->segment(3);

		$encaminhamentos = $this->encaminhamento->buscaPorId($id);
		$dados = array("encaminhamentos" => $encaminhamentos);

        $this->load->view('encaminhamentos/editar', $dados);
	}

	public function editar($id){
		$id = $this->uri->segment(3);

		date_default_timezone_set('America/Sao_Paulo');
    	$data_registro = date('d/m/Y H:i:s', time());
		
		$data = array(
			'id'				=> $id,
			'local' 			=> $_POST['local'],
			'data' 				=> $_POST['data'],
			'centro_custo' 		=> $_POST['centro_custo'],
			'tratamento' 		=> $_POST['tratamento'],
			'responsavel' 		=> $_POST['responsavel'],
			'cargo' 			=> $_POST['cargo'],
			'qtd_participantes' => $_POST['qtd_participantes'],
			'tb_usuarios_id' 	=> $this->session->userdata('usuario')->id,
			'servico' 			=> $_POST['servico'],
			'status'			=> 'Ativo',
			'data_registro'		=> $data_registro,
		);		
		$this->encaminhamento->editarEncaminhamento($data);
		$this->session->set_userdata('idEncaminhamento',$id);
		redirect('Itens/buscaPorId/'.$id);
	}	
}