<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {
	public function index()
	{    
        $this->load->library('session');
        $this->session->set_userdata('usuario', FALSE);
        $this->session->sess_destroy();
        $this->load->view('login');
    }   
}