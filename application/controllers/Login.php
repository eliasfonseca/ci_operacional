<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function index()
	{
		if ($this->session->has_userdata('usuario')) {
			redirect(base_url().'dashboard');
			exit();
		}
		$this->load->view('login');
	}

	public function autenticar()
	{
		$this->load->model("UsuariosModel", "usuario");
		$email = $_POST['email'];
		$senha = md5($_POST['senha']);

		$usuario = $this->usuario->logarUsuarios($email, $senha);

		if ($usuario) {
			$this->session->set_userdata('usuario',$usuario);
			redirect('dashboard');
		}else {
			$this->session->set_flashdata("msgDanger", "Login ou senha inválidos!");
			redirect('login');
		}

		exit();
	}
}