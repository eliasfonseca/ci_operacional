<?php if($this->session->has_userdata('msgInfo')) : ?>
	<div class="alert alert-info">
		<?php 
			echo $this->session->flashdata('msgInfo');
		?>
	</div>
<?php endif; ?>

<?php if($this->session->has_userdata('msgSuccess')) : ?>
	<div class="alert alert-success">
		<?php 
			echo $this->session->flashdata('msgSuccess');
		?>
	</div>
<?php endif; ?>

<?php if ($this->session->has_userdata('msgDanger')) : ?>
	<div class="alert alert-danger">
		<?php 
			echo $this->session->flashdata('msgDanger');
		?>
	</div>
<?php endif; ?>
