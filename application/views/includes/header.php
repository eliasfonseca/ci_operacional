<!doctype html>
<html lang="pt-br">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">


    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
  </head>
  <body>

  <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <img class="my-0 mr-md-auto font-weight-normal" src="<?php echo base_url('assets/imagens/logo.png'); ?>" alt="" width="100" height="">

    <nav class="my-2 my-md-0 mr-md-3">
      <a class="p-2 text-dark" href="<?php echo base_url('Encaminhamentos/listar'); ?>">Encaminhamentos</a>
      <a class="p-2 text-dark" href="<?php echo base_url('Empresas/listar'); ?>">Empresas</a>
      <a class="p-2 text-dark" href=""><b>Olá, <?php echo $this->session->userdata('usuario')->nome;?></b></a>
    </nav>
    <a class="btn btn-outline-primary" href="<?php echo base_url('Logout'); ?>">Sair</a> 

  </div>
