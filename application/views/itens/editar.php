<?php
    $this->load->view('includes/header');
?>

<div class="container">
      <div class="py-5 text-center">
        <h2>Editar Itens</h2>
      </div>
</div>

<div class="container">
        <div class="row">
            <div class="col-md-12 order-md-1">
            <form action="<?php echo base_url('Itens/editar') ?>" method="POST" id="formulario" class="needs-validation">

            <table class="table">
            <thead>
                <th>Id</th>
                <th>Item</th>
                <th>Observações</th>
                <th>#</th>
            </thead>
            <tbody>
            <?php foreach($itens as $item):?>
                <tr>
                    <td><?php echo $item->id; ?></td>
                    <td><?php echo $item->item; ?></td>
                    <td><?php echo $item->observacoes; ?></td>
                    <td>
                      <button type="button" id="<?php echo $item->id; ?>" data-id="<?php echo $item->id; ?>" class="btn btn-info edit" data-toggle="modal" data-target="#editarItem">Editar
                      </button>
                  </td>
                </tr>
            <?php endforeach ?>
            </tbody>
            </table>
                
            </form>


            <a class="btn btn-info btn-lg btn-block" href="<?php echo base_url('Orcamentos/buscaPorId/')?>" role="button">Próximo</a>
            </div>
        </div>
</div>



<form action="<?php echo base_url('Itens/editar/'); ?>" method="POST" id="form-item" class="needs-validation">
  <div class="modal fade" id="editarItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="idItem"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="#dadosCarregados" class="row">
                <div id="loading"></div>
                <input class="id_Item" type="hidden" name="id_Item">
                <div class="col-md-12">
                    <label for="cc-expiration">Item</label>

                    <input id="item" type="text" name="item" class="form-control" required>
                </div>

                <div class="col-md-12" style="margin-top: 10px;">
                    <label for="cc-expiration">Detalhamento (Use as teclas Enter ou  * para separar os detalhes)</label>
                    <select name="detalhes[]" multiple="multiple" class="col-md-12 js-example-tokenizer">
                    
                    </select>
                </div>          
                <div class="col-md-12" style="margin-top: 10px;">
                    <label for="cc-expiration">Observações</label>
                    <textarea id="observacoes" name="observacoes" rows="2" class="form-control">
                      
                    </textarea>
                </div>
                <input type="hidden" name="idEncaminhamento" value="<?php echo $this->session->userdata('idEncaminhamento') ?>">
            </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button> -->
          <button type="submit" class="btn btn-primary">Salvar alterações</button>
        </div>
      </div>
    </div>
  </div>
</form>


<?php
    $this->load->view('includes/footer');
?>

<script>
$(".js-example-tokenizer").select2({
    tags: true,
    tokenSeparators: ['*', '?']
})

$('.edit').on('click', function(){
  var _id = $(this).attr('data-id');

  //alert(_id);
  $.ajax({
    url: '<?= base_url(); ?>' + 'Itens/buscarItemModal',
    type: 'POST',
    dataType: "json",
    data:{'id': _id},
    beforeSend: function(){
      $("#loading").html("Carregando...");
    },
    success: function(item){
      console.log(item.id)
      $("#loading").html('')
      $(".modal-title").text('Id: '+item.id),
      $(".id_Item").val(item.id),
      $("#item").val(item.item),
      $("#detalhes").val(item.detalhes),
      $("#observacoes").val(item.observacoes)
    },
    error: function(data){
      $("#loading").html("Algum erro ocorreu aqui!");
    }
  });
});


$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})


</script>