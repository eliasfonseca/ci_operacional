<?php
    $this->load->view('includes/header');
?>

<div class="container">
      <div class="py-5 text-center">
        <h2>Adicionar Itens</h2>
      </div>
</div>

<div class="container">
<hr>
        <div class="row">
            <div class="col-md-12 order-md-1">
            <form action="<?php echo base_url('Itens/adicionar'); ?>" method="POST" id="formulario" class="needs-validation">
                <div class="row">
                <div class="col-md-3 mb-3">
                    <label for="cc-expiration">Encaminhamento</label>
                    <input name="idEncaminhamento" readonly="true" value="<?php echo $this->session->userdata('idEncaminhamento');?>" type="text" class="form-control" id="idEncaminhamento" placeholder="" required>
                </div>
                <div class="col-md-9 mb-3">
                    <label for="address2">Serviço</label>
                    <input name="servico" readonly="true" type="text" class="form-control" id="servico" placeholder="" value='<?php echo $this->session->userdata('servico');?>' required>
                </div>
                </div>

                <div style="margin-bottom: 20px; margin-top: 10px; padding: 15px; background-color: #dce3ea;">
                <div class="row">
                    <div class="col-md-12">
                        <label for="cc-expiration">Item</label>
                        <input id="item" type="text" name="item" class="form-control" required>
                    </div>

                    <div class="col-md-12" style="margin-top: 10px;">
                        <label for="cc-expiration">Detalhamento (Use as teclas Enter ou  * para separar os detalhes)</label>
                        <select name="detalhes[]" multiple="multiple" class="col-md-12 js-example-tokenizer">
                        
                        </select>
                    </div>              
                    <div class="col-md-12" style="margin-top: 10px;">
                        <label for="cc-expiration">Observações</label>
                        <textarea id="observacoes" name="observacoes" rows="2" class="form-control"></textarea>
                    </div>
                    <div class="col-md-12" style="text-align: right; margin-top: 10px;">
                    <button id="btnSalvarItem" class="btn-primary btn-sm">Salvar</button>
                    </div>
                </div>
                </div>
            </form>

            <a class="btn btn-info btn-lg btn-block" href="<?php echo base_url('Orcamentos/add');?>" role="button">Próximo</a>
            
            </div>
        </div>
</div>

<?php
    $this->load->view('includes/footer');
?>

<script>
$(".js-example-tokenizer").select2({
    tags: true,
    tokenSeparators: ['*', '?']
})
</script>