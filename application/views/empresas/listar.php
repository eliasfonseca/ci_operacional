<?php
    $this->load->view('includes/header');
?>

<div class="container">
      <div class="py-5 text-center">
        <h2>Empresas Cadastradas</h2>
      </div>

      <div class="row">
			<div id="listaEncaminhamentos" class="col-md-12 order-md-1">
				<p><a href="<?php echo base_url('Empresas/add') ?>" id="Editar" class="btn btn-success btn-sm">Novo</a></p>
				<table id="table_empresas" class="table display table-striped table-bordered table-condensed table-hover">
					<thead class="">
						<td>ID</td>
						<td>CNPJ</td>
						<td>Nome Empresa</td>
						<td>Nome Fantasia</td>
						<td>Editar</td>
						<td>Deletar</td>
					</thead>
					<?php foreach($empresas as $empresa) : ?>
						<tr>
							<td><?= $empresa->id; ?></td>
							<td><?= $empresa->cnpj; ?></td>
							<td><?= $empresa->nome; ?></td>
							<td><?= $empresa->nome_fantasia; ?></td>
							<td style="text-align:center;"><a class="btn btn-secondary btn-sm btn-color" href="<?php echo base_url('Empresas/buscaPorId/').$empresa->id; ?>">Editar</a></td>
							<td style="text-align:center;"><a onclick="return confirm('Deletar Permanente?')" class="btn btn-danger btn-sm btn-color" href="<?php echo base_url('Empresas/deletar/').$empresa->id; ?>">Deletar</a></td>
						</tr>
					<?php endforeach ?>
				</table>
			</div>
		</div>
</div>

<?php
    $this->load->view('includes/footer');
?>

<script>
	jQuery(document).ready( function () {
	  $('#table_empresas').DataTable();
	});
</script>