<?php
    $this->load->view('includes/header');
?>

<div class="container">
      <div class="py-5 text-center">
        <h2>Adicionar Nova Empresa</h2>
      </div>
</div>

<div class="container">
	<div class="row">
        <div class="col-md-12 order-md-1">
        <hr>
          <div id="mensagem"></div>
          <form id="formulario" class="needs-validation" action="<?php echo base_url('Empresas/adicionar'); ?>" method="POST">
          	<div class="row">
            <div class="col-md-3 mb-3">
                <label for="cc-expiration">CNPJ</label>
                <input name="cnpj" type="text" class="form-control cnpj" placeholder="" required>
            </div>

          	<div class="col-md-9 mb-3">
              <label for="address2">Razão Social</label>
              <input  name="nome-empresa" type="text" class="form-control nome-empresa" placeholder="" value="" required>
            </div>	            
            <div class="col-md-12 mb-3">
                <label for="cc-expiration">Nome Fantasia</label>
                <input name="nome-fantasia" type="text" class="form-control nome-fantasia" placeholder="" required>
            </div>
          </div>              

          </div>
	        </div>

            <hr class="mb-4">
            <div style="text-align:right;">
              <button id="salvar" class="btn btn-primary">Salvar</button>
            </div>
          </form>
        </div>
  </div>
</div>

<?php
    $this->load->view('includes/footer');
?>