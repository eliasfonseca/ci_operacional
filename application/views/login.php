<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login - Operacional</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="<?php base_url(); ?>assets/css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <form action="<?php base_url(); ?>login/autenticar" method="POST" class="form-signin">
      <img class="mb-4" src="<?php base_url(); ?>assets/imagens/logo.png" alt="">
      <h1 class="h3 mb-3 font-weight-normal">Login</h1>

	  <?php $this->load->view('includes/mensagens'); ?>

      <label for="usuario" class="sr-only">E-mail</label>
      <input type="email" name="email" id="email" class="form-control" placeholder="E-mail" required autofocus>
      <label for="inputPassword" class="sr-only">Senha</label>
      <input type="password" name="senha" id="senha" class="form-control" placeholder="Senha" required>

      <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
    </form>
  </body>
</html>
