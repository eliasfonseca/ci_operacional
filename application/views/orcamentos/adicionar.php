<?php
    $this->load->view('includes/header');
?>

<div class="container">
      <div class="py-5 text-center">
        <h2>Adicionar Orçamentos</h2>
      </div>
</div>

<body class="bg-light">
    <div class="container">
      <div class="row">
        <div class="col-md-4 order-md-2 mb-4">
        </div>
        <div class="col-md-12 order-md-1">
          <form action="<?php echo base_url('Orcamentos/adicionar'); ?>" method="POST" id="formulario" class="needs-validation">
            <div class="row">
              <div class="col-md-3 mb-3">
                  <label for="cc-expiration">Encaminhamento</label>
                  <input name="idEncaminhamento" readonly="true" value='<?php echo $user = $_SESSION['idEncaminhamento'];?>' type="text" class="form-control" id="idEncaminhamento" placeholder="" required>
              </div>
              <div class="col-md-9 mb-3">
                <label for="address2">Serviço</label>
                <input name="servico" readonly="true" type="text" class="form-control" id="servico" placeholder="" value='<?php echo $_SESSION['servico'] ?>' required>
              </div>        
            </div>

            <div class="itens" style="margin-bottom: 20px; margin-top: 10px; padding: 15px; background-color: #dce3ea;">
              <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="cc-expiration">Empresa</label>
                    <select class="form-control" name="empresa" id="empresa" required>
                        <option value="" selected="">Selecione a Empresa</option>'
                        <?php foreach($empresas as $empresa) : ?>
                            <option value="<?php echo $empresa->id; ?>"> <?php echo $empresa->nome_fantasia; ?></option>
                        <?php endforeach ?>
                    </select>
                </div> 
                <div class="col-md-3">
                    <label for="cc-expiration">Valor unitário</label>
                    <input id="valor_unitario" type="text" name="valor_unitario" class="valorFormatado form-control" required>
                </div>                  
                <div class="col-md-3">
                    <label for="cc-expiration">Valor extra</label>
                    <input id="valor_extra" type="text" name="valor_extra" class="valorFormatado form-control">
                </div>                  
                <div class="col-md-12">
                    <label for="cc-expiration">Observações</label>
                    <textarea name="observacoes" rows="4" class="form-control"></textarea>
                </div>
                <div class="col-md-12 btn-salvar" style="text-align: right; margin-top: 10px;">
                  <button id="btnSalvarItem" class="btn-primary btn-sm" style="text-align: right; margin-top: 10px;">Salvar</button>
                </div>
              </div>
            </div>
          </form>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-12">
              <table class="table table-sm">
                  <thead>
                    <tr>
                      <th>Nome Fantasia</th>
                      <th>CNPJ</th>
                      <th style="text-align: center;">Qtd. Participantes</th>
                      <th>Valor Unitário</th>
                      <th>Valor Extra</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                <tbody>
                  <?php foreach($orcamentos as $orcamento) : ?>
                      <tr>
                        <td><?php echo $orcamento->nome_fantasia; ?></td>
                        <td><?php echo $orcamento->cnpj; ?></td>
                        <td style="text-align: center;"><?php echo $orcamento->qtd_participantes; ?></td>
                        <td><?php echo 'R$'. number_format($orcamento->valor_unitario,2,",","."); ?></td>
                        <td><?php echo 'R$'. number_format($orcamento->valor_extra,2,",","."); ?></td>
                        <td>
                          <?php echo 'R$'. number_format(($orcamento->qtd_participantes * $orcamento->valor_unitario) + $orcamento->valor_extra,2,",",".");?>
                        </td>
                      </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <a class="btn btn btn-info btn-lg btn-block" href="<?php echo base_url('Encaminhamentos/listar');?>" role="button">Finalizar</a>
      </div>
    </div>
  </div>


<?php
    $this->load->view('includes/footer');
?>

<script>
   $('.valorFormatado').mask('###0.00', {reverse: true});
</script>