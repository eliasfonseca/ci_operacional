<?php
    $this->load->view('includes/header');
?>

<div class="container">
      <div class="py-5 text-center">
        <h2>Encaminhamentos Cadastrados</h2>
      </div>


      <div class="row">
        <div id="listaEncaminhamentos" class="col-md-12 order-md-1">
          <p><a href="<?php echo base_url('Encaminhamentos/add') ?>" class="btn btn-success btn-sm">Novo</a></p>
          <div id="encaminhamentos">
                <table id="data-table" class="table display table-striped table-bordered table-condensed table-hover">
                  <thead class="">
                    <td>ID/Ano</td>
                    <td>Serviço</td>
                    <td>Centro de Custo</td>
                    <td>Responsável</td>
                    <td>Data</td>
                    <td>Operador</td>
                    <td>Relatório</td>
                    <td>Editar</td>
                    <td>Deletar</td>
                  </thead>

                  <tbody>

                  <?php foreach ($encaminhamentos as $encaminhamento): ?>
                    <tr>
                        <td class="chave">
                            <?php echo $encaminhamento->id. '/'. date('Y'); ?>
                        </td>
                        <td class="servico">
                            <?php echo $encaminhamento->servico; ?>
                        </td>
                        <td class="centro_custo">
                            <?php echo $encaminhamento->centro_custo; ?>
                        </td>
                        <td class="operador">
                            <?php echo $encaminhamento->responsavel; ?>
                        </td>
                        <td class="data">
                            <?php echo $encaminhamento->data; ?>
                        </td>
                        <td class="responsavel">
                            <?php echo $this->session->userdata('usuario')->nome; ?>
                        </td>
                        <td align="center">
                            <a href="" class="btn btn-success btn-sm" id="btn-editar">Relatório</a>
                        </td>
                        <td align="center">
                            <a href="<?php echo base_url('Encaminhamentos/buscaPorId/').$encaminhamento->id;?>" class="btn btn-secondary btn-sm btn-color" id="btn-editar">Editar</a>
                        </td>
                        <td align="center">
                            <a onclick="return confirm('Deletar Permanente?')" href="<?php echo base_url('Encaminhamentos/deletar/').$encaminhamento->id; ?>" class="btn btn-danger btn-sm btn-color" id="btn-editar">Deletar</a>
                        </td>
                    </tr>
                   <?php endforeach ?>

                  </tbody>
                </table>
          </div>
        </div>
      </div>

</div>

<?php
    $this->load->view('includes/footer');
?>

<script>
	jQuery(document).ready( function () {
	  $('#data-table').DataTable();
	});
</script>