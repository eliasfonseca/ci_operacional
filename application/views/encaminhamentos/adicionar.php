<?php
    $this->load->view('includes/header');
?>

<div class="container">
    <div class="py-5 text-center">
        <h2>Novo Encaminhamento</h2>
    </div>
</div>

<div class="container">
	<div class="row">
        <div class="col-md-12 order-md-1">
            <hr>
            <form action="<?php echo base_url('Encaminhamentos/adicionar');?>" method="POST" class="needs-validation">
                <div class="row">
                    <div class="col-md-9 mb-3">
                        <label for="address2">Local do Evento</label>
                        <input name="local" type="text" class="form-control" id="address2" placeholder="" value="" required>
                    </div>
                    <div class='col-sm-3 mb-3 date' id='datetimepicker1'>
                        <label for="address2">Data do Evento</label>
                        <input name="data" type='date' class="form-control" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <label for="lastName">Centro de Custo</label>
                        <input name="centro_custo" type="text" class="form-control" id="lastName" placeholder="" value="" required>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="cc-expiration">Tratamento</label>
                        <input name="tratamento" type="text" class="form-control" id="cc-expiration" placeholder="" required>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="cc-expiration">Responsável</label>
                        <input name="responsavel" type="text" class="form-control" id="cc-expiration" placeholder="" required>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="cc-expiration">Cargo</label>
                        <input name="cargo" type="text" class="form-control" id="cc-expiration" placeholder="" required>
                    </div>	            
                    <div class="col-md-6 mb-3">
                        <label for="cc-expiration">Qtd. Participantes</label>
                        <input name="qtd_participantes" type="text" class="form-control" id="cc-expiration" placeholder="" required>
                    </div>	            
                    <div class="col-md-6 mb-3">
                        <label for="cc-expiration">Operador</label>
                        <input readonly="true" value='<?php echo $this->session->userdata('usuario')->nome ?>' name="operador" type="text" class="form-control" id="cc-expiration" placeholder="" required>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="cc-expiration">Serviço</label>
                        <textarea name="servico" rows="4" class="form-control" required></textarea>
                    </div>
                </div>
                <hr class="mb-4">
                <button id="btnSalvarItem" class="btn btn btn-info btn-lg btn-block">Salvar e Próximo</button>
            </form>
        </div>
    </div>
</div>

<?php
    $this->load->view('includes/footer');
?>