<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class EncaminhamentosModel extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function listar()
	{
        $this->db->where("status !=", 'Desabilitado');
        $query = $this->db->get('tb_encaminhamentos');
		//$this->db->order_by("id", "desc");
        return $query->result();
    }

    public function deletarEncaminhamento($id)
    {
        $this->db->where('id', $id);
        $this->db->update('tb_encaminhamentos', array('status' => 'Desabilitado'));
    }

    public function adicionarEncaminhamento($data)
    {
        $this->db->insert('tb_encaminhamentos', $data);
        return $this->db->insert_id();
    }

    public function buscaPorId($id){
        $this->db->where("id", $id);
        $query = $this->db->get('tb_encaminhamentos');
        return $query->result();
    }

    public function editarEncaminhamento($data){
        $this->db->where('id', $data['id']);
        $this->db->update('tb_encaminhamentos', $data);
    }
}