<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class EmpresasModel extends CI_Model{
	public function listar(){
        $this->db->where("status !=", 'Desabilitado');
		$query = $this->db->get('tb_empresas');
        return $query->result();
    }

    public function adicionarEmpresa($data){
        $this->db->insert("tb_empresas", $data);
    }

    public function deletarEmpresa($id){
        $this->db->where('id', $id);
        $this->db->update('tb_empresas', array('status' => 'Desabilitado'));
    }

    public function editarEmpresa($id, $data){
        $this->db->where('id', $id);
        $this->db->update('tb_empresas', $data);
    }

    public function buscaPorId($id){
        $this->db->where("id", $id);
        $query = $this->db->get('tb_empresas');
        return $query->result();
    }
}