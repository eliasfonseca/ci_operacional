<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class OrcamentosModel extends CI_Model{
	public function adicionarOrcamento($data){
		$this->db->insert("tb_orcamentos", $data);
	}

	public function previaOrcamentos(){

		$this->db->select('o.*, e.nome_fantasia, e.cnpj, en.qtd_participantes');
		$this->db->join('tb_empresas e', 'e.id = o.tb_empresas_id');
		$this->db->join('tb_encaminhamentos en', 'en.id = o.tb_encaminhamento_id');

		$this->db->where('o.tb_encaminhamento_id', $this->session->userdata('idEncaminhamento'));
		$this->db->where('e.status', 'Ativo');

		$this->db->order_by("o.id", "desc");
		$query = $this->db->get('tb_orcamentos o');
		return $query->result();

	}
}