<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class ItensModel extends CI_Model{
        public function adicionarItens($data){
                $this->db->insert("tb_itens", $data);

        }

        public function editarItem($data){
                // echo '<pre>';
                // print_r($data);
                // exit();
                $this->db->where('id', $data['id_Item']);
                unset($data['id_Item']);
                $this->db->update('tb_itens', $data);
        }

        public function buscaPorId($id){

                $this->db->select('i.*, e.servico');
                $this->db->join('tb_encaminhamentos e', 'e.id = i.tb_encaminhamento_id');

                $this->db->where('i.tb_encaminhamento_id', $id);
                $query = $this->db->get('tb_itens i');
                return $query->result();
        }

        public function buscarItem($id){
                $this->db->where("id", $id);
                $query = $this->db->get('tb_itens');
                return $query->row();
        }

}