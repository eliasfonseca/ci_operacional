<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class UsuariosModel extends CI_Model{

    public function adicionar($usuario){
        $this->db->insert("tb_usuarios", $usuario);
    }

	public function logarUsuarios($email, $senha)
	{
        $this->db->where("email", $email);
        $this->db->where("senha", $senha);
        $usuario  = $this->db->get("tb_usuarios")->row();
        return $usuario;
    }
}