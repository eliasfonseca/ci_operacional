-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2018 at 02:43 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_operacional`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_empresas`
--

CREATE TABLE `tb_empresas` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `nome_fantasia` varchar(255) NOT NULL,
  `cnpj` varchar(255) NOT NULL,
  `status` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_empresas`
--

INSERT INTO `tb_empresas` (`id`, `nome`, `nome_fantasia`, `cnpj`, `status`) VALUES
(1, 'S.A.', 'BANCO DO BRASIL', '00.000.000/0001-91', 'Desabilitado'),
(2, 'BANCO', 'BRASIL', '91', 'Desabilitado'),
(5, 'BANCO DO BRASIL S.A.', 'BANCO DO BRASIL', '00.000.000/0001-91', 'Ativo'),
(6, 'BRASIL', 'BRASIL', '00.000.000/0001-91', 'Ativo'),
(7, 'BANCO SANTANDER', 'SANTANDER', '00.000.000/0001-22', 'Ativo');

-- --------------------------------------------------------

--
-- Table structure for table `tb_encaminhamentos`
--

CREATE TABLE `tb_encaminhamentos` (
  `id` int(11) NOT NULL,
  `local` varchar(255) NOT NULL,
  `data` datetime NOT NULL,
  `centro_custo` varchar(45) NOT NULL,
  `tratamento` varchar(45) NOT NULL,
  `responsavel` varchar(45) NOT NULL,
  `cargo` varchar(45) NOT NULL,
  `servico` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `qtd_participantes` int(11) NOT NULL,
  `data_registro` datetime DEFAULT NULL,
  `tb_usuarios_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_encaminhamentos`
--

INSERT INTO `tb_encaminhamentos` (`id`, `local`, `data`, `centro_custo`, `tratamento`, `responsavel`, `cargo`, `servico`, `status`, `qtd_participantes`, `data_registro`, `tb_usuarios_id`) VALUES
(108, 'São Paulo', '2018-11-22 00:00:00', 'Central de Dados', 'Sr.', 'Fulano', 'Estagiário', 'Teste', 'Ativo', 10, '0000-00-00 00:00:00', 1),
(109, 'São Paulo - SP', '2018-11-23 00:00:00', 'Central de Dados', 'Sr.', 'Fulano', 'Estagiário', 'Teste', 'Ativo', 50, '0000-00-00 00:00:00', 1),
(110, 'Brasíliav', '2018-11-29 00:00:00', 'Central de Dados', 'Sr.', 'Fulano', 'Estagiário', 'teste', 'Ativo', 50, '0000-00-00 00:00:00', 1),
(111, 'Brasília', '2018-11-28 00:00:00', 'asdfasd', 'dsfasdf', 'asdfasdf', 'asdfasdf', 'asdf', 'Ativo', 50, '0000-00-00 00:00:00', 1),
(112, 'Brasília', '2018-11-28 00:00:00', 'asdfasd', 'dsfasdf', 'asdfasdf', 'asdfasdf', 'asdf', 'Ativo', 50, '0000-00-00 00:00:00', 1),
(113, 'Brasília', '2018-11-28 00:00:00', 'asdfasd', 'dsfasdf', 'asdfasdf', 'asdfasdf', 'asdf', 'Ativo', 50, '0000-00-00 00:00:00', 1),
(114, 'Brasíliav', '2018-11-30 00:00:00', 'Central de Dados', 'Sr.', 'Fulano', 'Estagiário', 'afasdf', 'Ativo', 10, '0000-00-00 00:00:00', 1),
(115, 'Brasília', '2018-11-28 00:00:00', 'Estudos Técnicos', 'Sr.', 'Jhonatan Lindão', 'Programador', 'Seminário', 'Ativo', 55, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_itens`
--

CREATE TABLE `tb_itens` (
  `id` int(11) NOT NULL,
  `item` varchar(255) NOT NULL,
  `detalhes` text NOT NULL,
  `observacoes` text NOT NULL,
  `tb_encaminhamento_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_itens`
--

INSERT INTO `tb_itens` (`id`, `item`, `detalhes`, `observacoes`, `tb_encaminhamento_id`) VALUES
(106, 'Seminário', 'a', 'Teste', 108),
(107, 'Jantararrr', 'a|b|c', 'Testeasdfasdfasdf', 108),
(108, 'Almoço', 'Água|Suco', 'Teste', 109),
(109, 'Jantar', 'Água|Café', 'teste', 109),
(110, 'Seminário a', 'a|b|c', 'teste', 115),
(111, 'Almoço', 'a|b|c|d', 'teste', 115);

-- --------------------------------------------------------

--
-- Table structure for table `tb_orcamentos`
--

CREATE TABLE `tb_orcamentos` (
  `id` int(11) NOT NULL,
  `valor_unitario` double NOT NULL,
  `valor_extra` double DEFAULT NULL,
  `observacoes` text,
  `status` varchar(255) NOT NULL,
  `tb_empresas_id` int(11) NOT NULL,
  `tb_encaminhamento_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_orcamentos`
--

INSERT INTO `tb_orcamentos` (`id`, `valor_unitario`, `valor_extra`, `observacoes`, `status`, `tb_empresas_id`, `tb_encaminhamento_id`) VALUES
(92, 100, 1000, 'Teste', 'Ativo', 5, 108),
(93, 50, 25, 'Teste', 'Ativo', 7, 108),
(94, 100, 1000, 'teste', 'Ativo', 5, 109),
(95, 100, 25, 'teste', 'Ativo', 6, 109),
(96, 100, 100, 'teste', 'Ativo', 5, 115),
(97, 500, 200, 'teste', 'Ativo', 6, 115);

-- --------------------------------------------------------

--
-- Table structure for table `tb_usuarios`
--

CREATE TABLE `tb_usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL,
  `data_cadastro` varchar(45) NOT NULL,
  `nivel_permissao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_usuarios`
--

INSERT INTO `tb_usuarios` (`id`, `nome`, `email`, `senha`, `status`, `data_cadastro`, `nivel_permissao`) VALUES
(1, 'Elias Fonseca', 'elias.santiago@cnm.org.br', '202cb962ac59075b964b07152d234b70', '1', '20/09/2018', 1),
(3, 'Fulano', 'fulano@email.com', '202cb962ac59075b964b07152d234b70', '0', '05/11/2018', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_empresas`
--
ALTER TABLE `tb_empresas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_encaminhamentos`
--
ALTER TABLE `tb_encaminhamentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tb_encaminhamento_tb_usuarios_idx` (`tb_usuarios_id`);

--
-- Indexes for table `tb_itens`
--
ALTER TABLE `tb_itens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tb_itens_tb_encaminhamento1_idx` (`tb_encaminhamento_id`);

--
-- Indexes for table `tb_orcamentos`
--
ALTER TABLE `tb_orcamentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tb_orcamentos_tb_empresas1_idx` (`tb_empresas_id`),
  ADD KEY `fk_tb_orcamentos_tb_encaminhamento1_idx` (`tb_encaminhamento_id`);

--
-- Indexes for table `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_empresas`
--
ALTER TABLE `tb_empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_encaminhamentos`
--
ALTER TABLE `tb_encaminhamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `tb_itens`
--
ALTER TABLE `tb_itens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `tb_orcamentos`
--
ALTER TABLE `tb_orcamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_encaminhamentos`
--
ALTER TABLE `tb_encaminhamentos`
  ADD CONSTRAINT `fk_tb_encaminhamento_tb_usuarios` FOREIGN KEY (`tb_usuarios_id`) REFERENCES `tb_usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_itens`
--
ALTER TABLE `tb_itens`
  ADD CONSTRAINT `fk_tb_itens_tb_encaminhamento1` FOREIGN KEY (`tb_encaminhamento_id`) REFERENCES `tb_encaminhamentos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `tb_orcamentos`
--
ALTER TABLE `tb_orcamentos`
  ADD CONSTRAINT `fk_tb_orcamentos_tb_empresas1` FOREIGN KEY (`tb_empresas_id`) REFERENCES `tb_empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_orcamentos_tb_encaminhamento1` FOREIGN KEY (`tb_encaminhamento_id`) REFERENCES `tb_encaminhamentos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
